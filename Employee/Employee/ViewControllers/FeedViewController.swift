//
//  FeedViewController.swift
//  Employee
//
//  Created by admin on 23/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class FeedViewController: BaseViewController {
    
    var feeds: [Feed] = []

    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.delegate = self
        TableView.dataSource = self

    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadMessages()
    }
    
    func loadMessages()
    {
        feeds.removeAll()
        
        makeApiCall(path: "/services/",
                    completionHandler: {result in

                let tempFeed = result as! [[String: Any]]
                for object in tempFeed {
                    let message = object["message"] as! String
                    let role = object["role"] as! String
                    let feed = Feed(message: message, role: role)
                    self.feeds.append(feed)
                }
                        
                self.TableView.reloadData()
        }, method: .get)
    }
    
    func deleteMessage(indexPath: Int)
    {
        
    }
    
   
//    @IBAction func Clear() {
//        numbers.remove(at: indexPath.row)
//        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
//    }
    
}

extension FeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell")!
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell")!
        
        let labelRole = cell.viewWithTag(1) as! UILabel
        let labelMessage = cell.viewWithTag(2) as! UILabel
        //let buttonClear = cell.viewWithTag(3) as! UIButton
        
        let feed = feeds[indexPath.row]
        labelRole.text = feed.role
        labelMessage.text = feed.message
        func buttonClear() {
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    
    let actionDelete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
        self.feeds.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.reloadData()
    }
        return [actionDelete]
    }
    
//     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            objects.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }
    
    
}

extension FeedViewController: UITableViewDelegate {
    
}
