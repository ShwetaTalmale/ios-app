//
//  SigninViewController.swift
//  Employee
//
//  Created by admin on 23/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Alamofire


class SigninViewController: BaseViewController {

    @IBOutlet weak var editEmail: UITextField!
    @IBOutlet weak var editPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func onLogin() {
        if editEmail.text!.count == 0 {
            showError(message: "Email is mandatory")
        } else if editPassword.text!.count == 0 {
            showError(message: "Password is mandatory")
        } else {
            print(editEmail.text!)
            email1 = editEmail.text!
            let body = [
                "empemail": editEmail.text!,
                "password": editPassword.text!
            ]
            
            //let url = "http://172.18.5.213:4000/employee/login"
            let url = "http://192.168.43.89:4000/employee/login"
            AF.request(url, method: .post, parameters: body,encoding: JSONEncoding())
                .responseJSON(completionHandler: { response in
                        let result = response.value as! [String: Any]
                                    let status = result["status"] as! String
                    //print("\(result)")
                    
                    if status == "success" {

                        let data = result["data"] as! [String: Any]
                        let id = data["empid"] as! Int
                        let name = data["empusername"] as! String
                        let empemail = data["empemail"] as! String

                        // persist the userId in user defaults
                        let userDefaults = UserDefaults.standard
                        userDefaults.setValue(id, forKey: "empid")
                        userDefaults.setValue(name, forKey: "empusername")
                        userDefaults.setValue(empemail, forKey: "empemail")
                        userDefaults.synchronize()
                        
                        print(data)

                        let tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "tabBarController")
                        self.navigationController?.pushViewController(tabBarController, animated: true)
                        
                        let xyz = Email.init(email: self.editEmail.text!).email
                        print(xyz!)

                    } else {
                        self.showError(message: "Invalid email or pasword")
                   }
                })
        }
    }
    
}
