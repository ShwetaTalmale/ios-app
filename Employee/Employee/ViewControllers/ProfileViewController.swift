//
//  ProfileViewController.swift
//  Employee
//
//  Created by admin on 23/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ProfileViewController: BaseViewController {
    
    var employee: [Employee] = []

    @IBOutlet weak var salary: UILabel!
    @IBOutlet weak var birth: UILabel!
    @IBOutlet weak var joining: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phoneno: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var empid: UILabel!
    @IBOutlet weak var lastname: UILabel!
    @IBOutlet weak var firstname: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("EMAIL :: \(email1)")
        
        
        
        makeApiCall(path: "/employee/join/\(UserDefaults.standard.object(forKey: "empemail")!)", completionHandler: { result in
            
            
            let array = result as! [[String: Any]]
            for item in array{
                let add  = Employee()
                add.firstname = item["empfirstname"] as? String
                add.lastname = item["emplastname"]as? String
                add.category = item["categories"]as? String
                add.empid = item["empid"]as? Int
                add.email = item["empemail"]as? String
                add.phoneno = item["empcontact"]as? Int
                add.joining = item["dateofjoining"]as? String
                add.birth = item["dateofbirth"]as? String
                add.salary = item["salary"]as? Int
                add.thumbnail = item["image"]as? String
                self.employee.append(add)
                
                let url1 = URL(string: self.urls + "/\(add.thumbnail!)" )
                self.imageView.kf.setImage(with: url1)
            }
            
            //print(self.employee[1].firstname!)
            print(self.employee)
            self.firstname.text = self.employee[0].firstname!
            self.lastname.text = self.employee[0].lastname!
            self.role.text = self.employee[0].category!
            self.empid.text = String(self.employee[0].empid!)
            self.email.text = self.employee[0].email!
            self.phoneno.text = String(self.employee[0].phoneno!)
            self.joining.text = self.employee[0].joining!
            self.birth.text = self.employee[0].birth!
            self.salary.text = String(self.employee[0].salary!)
            
            
            
            
        }, method: .get)
        }

    
    @IBAction func onLogout() {
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        
        UserDefaults.standard.synchronize()
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SigninViewController")
        self.navigationController?.popViewController( animated: true)
        //present(vc, animated: true, completion: nil)
    }
    
}
