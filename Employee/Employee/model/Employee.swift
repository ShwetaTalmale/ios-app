//
//  Employee.swift
//  Employee
//
//  Created by admin on 23/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
class Employee{
    var firstname: String!
    var lastname: String!
    var category: String!
    var empid: Int!
    var email:String!
    var phoneno: Int!
    var joining:String!
    var birth: String!
    var salary: Int!
    var thumbnail: String!
}
