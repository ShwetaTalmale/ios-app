//
//  HomeViewController.swift
//  User
//
//  Created by admin on 20/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = UserDefaults.standard.object(forKey: "username") as! String
    }
   

}
