//
//  OrderViewController.swift
//  User
//
//  Created by admin on 20/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire

class OrderViewController: BaseViewController {
    
    
    var code: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) {
            (granted, error) in
            if granted {
                print("yes")
            } else {
                print("No")
            }
        }

        
    }
    

    @IBAction func onHousekeeping() {
        let msg1 = "Room No \(code!) wants House Keeping Service"
        
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Flora"
        content.subtitle = "for HouseKeeping"
        content.body = msg1

        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        let body: [String: Any] = [
            "message": msg1,
            "role": "House Keeping",
        ]
        makeApiCall(path: "/services", completionHandler: { result in
            let alert = UIAlertController(title: "success", message: "Your request is delivered..", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            
        }, method: .post, parameters: body)
        self.showSuccess(message: "Request Success")
    }
    
    @IBAction func Breakfast() {
        let msg1 = "Room No \(code!) Ordered Breakfast"
        
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Flora"
        content.subtitle = "for HouseKeeping"
        content.body = msg1

        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        let body: [String: Any] = [
            "message": msg1,
            "role": "chef",
        ]
        makeApiCall(path: "/services", completionHandler: { result in
            let alert = UIAlertController(title: "success", message: "Your request is delivered..", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            
        }, method: .post, parameters: body)
        self.showSuccess(message: "Request Success")
    }
    
    @IBAction func Lunch() {
        let msg1 = "Room No \(code!) ordered Lunch"
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Flora"
        content.subtitle = "for HouseKeeping"
        content.body = msg1

        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        let body: [String: Any] = [
            "message": msg1,
            "role": "Chef",
        ]
        makeApiCall(path: "/services", completionHandler: { result in
            let alert = UIAlertController(title: "success", message: "Your request is delivered..", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            
        }, method: .post, parameters: body)
        self.showSuccess(message: "Request Success")
    }
    
    
    @IBAction func Dinner() {
        let msg1 = "Room No \(code!) Ordered Dinner"
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Flora"
        content.subtitle = "for HouseKeeping"
        content.body = msg1

        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        
        let body: [String: Any] = [
            "message": msg1,
            "role": "Chef",
        ]
        makeApiCall(path: "/services", completionHandler: { result in
            let alert = UIAlertController(title: "success", message: "Your request is delivered..", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            
        }, method: .post, parameters: body)
        self.showSuccess(message: "Request Success")
    }
    
    @IBAction func Valet() {
        let msg1 = "Room No \(code!) Requested for Valet"
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Flora"
        content.subtitle = "for HouseKeeping"
        content.body = msg1

        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        
        let body: [String: Any] = [
            "message": msg1,
            "role": "Valet",
        ]
        makeApiCall(path: "/services", completionHandler: { result in
            let alert = UIAlertController(title: "success", message: "Your request is delivered..", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            
        }, method: .post, parameters: body)
        self.showSuccess(message: "Request Success")
    }
    
    @IBAction func Waiter() {
        let msg1 = "Room No \(code!) Requested for Waiter"
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Flora"
        content.subtitle = "for HouseKeeping"
        content.body = msg1

        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        
        
        let body: [String: Any] = [
            "message": msg1,
            "role": "Waiter",
        ]
        makeApiCall(path: "/services", completionHandler: { result in
            let alert = UIAlertController(title: "success", message: "Your request is delivered..", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            
        }, method: .post, parameters: body)
        self.showSuccess(message: "Request Success")
    }
}
