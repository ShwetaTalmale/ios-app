//
//  ServicesViewController.swift
//  User
//
//  Created by admin on 20/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class ServicesViewController: BaseViewController {

    @IBOutlet weak var editCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func onsubmit() {
        
        let room = ["R101":"101", "R102":"102", "R103":"103", "R104":"104", "R105":"105"]
        
        
        
     if editCode.text! == "101" || editCode.text! == "102" || editCode.text! == "103" || editCode.text! == "104" || editCode.text! == "105"{
            
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OrderViewController") as! OrderViewController
        
        vc.code = editCode.text!
        
        //present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.showError(message: "Invalid Code ")
        }
    }
}
