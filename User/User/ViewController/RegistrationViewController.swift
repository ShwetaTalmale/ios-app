//
//  RegistrationViewController.swift
//  User
//
//  Created by admin on 18/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class RegistrationViewController: BaseViewController {

    @IBOutlet weak var editPhone: UITextField!
    @IBOutlet weak var editPassword: UITextField!
    @IBOutlet weak var editUsername: UITextField!
    @IBOutlet weak var editEmail: UITextField!
    @IBOutlet weak var editLastname: UITextField!
    @IBOutlet weak var editFirstname: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Register User"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(onRegister))
       
    }
    
    @objc func onRegister() {
        if editFirstname.text!.count == 0 {
            showError(message: "First Name is mandatory")
        } else if editPassword.text!.count == 0 {
            showError(message: "Password is mandatory")
        } else if editPhone.text!.count == 0 {
            showError(message: "Phone is mandatory")
        } else if editEmail.text!.count == 0 {
            showError(message: "Email is mandatory")
        } else {
            
            let body = [
                "firstname": editFirstname.text!,
                "lastname": editLastname.text!,
                "username": editUsername.text!,
                "email": editEmail.text!,
                "password": editPassword.text!,
                "contact": editPhone.text!
            ]
            
            makeApiCall(path: "/user/register",
                        completionHandler: { result in
                        
                            let alert = UIAlertController(title: "success", message: "Registered a new user. Please login now.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                                
                        }, method: .post, parameters: body)
            
        }
    }



}
