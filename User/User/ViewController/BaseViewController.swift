//
//  BaseViewController.swift
//  User
//
//  Created by admin on 18/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Alamofire

class BaseViewController: UIViewController {
    let urls = "http://192.168.43.89:4000"
    //let urls = "http://172.18.5.213:4000"

    var email1: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    func showError(message: String) {
        let alert = UIAlertController(title: "error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func showSuccess(message: String) {
        let alert = UIAlertController(title: "success", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func makeApiCall(path: String,  completionHandler: @escaping (Any?) -> Void, method: HTTPMethod = .get, parameters: Parameters? = nil) {
        let url = urls + path
        print("calling API: \(url)")
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding())
            .responseJSON(completionHandler: { response in
                print("RESPONSE  :: \(response)")
                let result = response.value as! [String: Any]
                let status = result["status"] as! String
                if status == "success" {
                    completionHandler(result["data"])
                } else {
                    print("Error while calling API: \(result["error"]!)")
                }
            })
    }
    
}
