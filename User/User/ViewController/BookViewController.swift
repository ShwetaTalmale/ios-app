//
//  BookViewController.swift
//  User
//
//  Created by admin on 20/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Alamofire


class BookViewController: BaseViewController{

    @IBOutlet weak var checkoutDate: UIDatePicker!
    @IBOutlet weak var checkinDate: UIDatePicker!
   
    @IBOutlet weak var beddingPicker: UIPickerView!
    @IBOutlet weak var roomtypePicker: UIPickerView!
    
    var categories: [String] = []
    var categoryid: [Int] = []
    
    var bedding: [String] = ["Single", "Double", "Triple", "Quad"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //roomtypePicker.reloadAllComponents()
        beddingPicker.delegate = self
        beddingPicker.dataSource = self
        
        makeApiCall(path: "/roomcategory/",
                               completionHandler: { result in
                               
                                //print("result:\(result)")
                                let tempcategory = result as! NSArray;                            // print("tempcategory:\(tempcategory)")
                                for object in tempcategory {
                                    let cat = object as! NSDictionary
                                    self.categories.append(cat.value(forKey: "category") as! String)
                                    self.categoryid.append(cat.value(forKey: "categoryid") as! Int)
                                    //self.categories1.append(cat)
                                }
                                self.roomtypePicker.reloadComponent(0)
                                //print(self.categories)
                                //print(self.categoryid)
                                }, method: .get, parameters: nil)
        
    }
    

    @IBAction func onBook() {
        
        print("EMAIL ::\(email1)")
        
        let checkin = checkinDate.date
        let checkout = checkoutDate.date
        
        let row = roomtypePicker.selectedRow(inComponent: 0)
        let selectedCategory = categories[row]
        
        let row1 = beddingPicker.selectedRow(inComponent: 0)
        let selectedCategory1 = bedding[row1]
        
         let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"

         let inDate = formatter.string(from: checkin)
         let outDate = formatter.string(from: checkout)
        
        let body: [String: Any] = [
            "check_in_date": inDate,
            "check_out_date": outDate,
            //"categoryid": categoryid[find(categories, "selectedCategory")],
            "categoryid": categoryid[categories.firstIndex(of: selectedCategory)!],
            "beddingtype": selectedCategory1]
        
       // print("BODY :: \(body)")
    
        makeApiCall(path: "/bookingdetails/book", completionHandler: { result in
            
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PaymentViewController") as! PaymentViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            //self.present(vc, animated: true, completion: nil)
            self.showSuccess(message: "Booking Success")
        }, method: .post, parameters: body)
    }
}

extension BookViewController: UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if(pickerView === roomtypePicker)
        {
            return 1
        }
        else if(pickerView === beddingPicker)
        {
            return 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView === roomtypePicker)
        {
            return categories.count
        }
        else if(pickerView === beddingPicker){
            return bedding.count
        }
        return 0
    }
}

extension BookViewController: UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       if(pickerView === roomtypePicker)
        {
            return categories[row]
        }
        else if(pickerView === beddingPicker){
            return bedding[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
//    if(pickerView === roomtypePicker)
//           {
//    selectedCategory = categories[row]
//            print(selectedCategory)
//           }
//           else if(pickerView === beddingPicker){
//            selectedBedding = bedding[row]
//        print(selectedBedding)
//    }
        }
    
}
