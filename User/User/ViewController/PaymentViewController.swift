//
//  PaymentViewController.swift
//  User
//
//  Created by admin on 20/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class PaymentViewController: BaseViewController {

    @IBOutlet weak var editMode: UITextField!
    @IBOutlet weak var editAmount: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var editStatus: UITextField!
    
    @IBOutlet weak var editEmail: UITextField!
    
   var id: [Int] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeApiCall(path: "/bookingdetails/id", completionHandler: { result in
            let tempid = result as! NSArray;
            for object in tempid {
                let cat = object as! NSDictionary
                self.id.append(cat.value(forKey: "bookid") as! Int)
        
             }
         
             } ,method: .get, parameters: nil)
        
        
    }
    
    
    
    @IBAction func onPay() {
        let date = datePicker.date
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        
        let payDate = formatter.string(from: date)
        
        let body: [String: Any] = [
            "bookid": id[0],
        "billingmode": editMode.text!,
        "amount": editAmount.text!,
        "billingdate": payDate,
        "status": editStatus.text!]
        
        print("BODY :: \(body)")
        
        makeApiCall(path: "/billing", completionHandler: { result in
                   
                   
                   let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "HomeViewController") as! HomeViewController
                   
            //self.present(vc, animated: true, completion: nil)
                   self.navigationController?.pushViewController(vc, animated: true)
                   self.showSuccess(message: "Payment Success")
               }, method: .post, parameters: body)
    }
}
