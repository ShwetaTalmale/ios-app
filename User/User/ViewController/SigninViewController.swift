//
//  SigninViewController.swift
//  User
//
//  Created by admin on 18/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Alamofire

class SigninViewController: BaseViewController {

    @IBOutlet weak var editPassword: UITextField!
    @IBOutlet weak var editEmail: UITextField!
    
    var email12:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func onLogin() {
        
            if editEmail.text!.count == 0 {
                showError(message: "Email is mandatory")
            } else if editPassword.text!.count == 0 {
                showError(message: "Password is mandatory")
            } else {
                print(editEmail.text!)
                email1 = editEmail.text!
                let body = [
                    "email": editEmail.text!,
                    "password": editPassword.text!
                ]
                
                //let url = "http://172.18.5.213:4000/user/login"
                let url = "http://192.168.43.89:4000/user/login"
                AF.request(url, method: .post, parameters: body,encoding: JSONEncoding())
                    .responseJSON(completionHandler: { response in
                            let result = response.value as! [String: Any]
                                        let status = result["status"] as! String
                        //print("\(result)")
                        
                        if status == "success" {

                            let data = result["data"] as! [String: Any]
                            let id = data["userid"] as! Int
                            let username = data["username"] as! String
                            //let firstname = data["firstname"] as! String
                            //let lastname = data["lastname"] as! String
                            self.email12 = data["email"] as! String
                            //let contact = data["contact"] as! String
                            
                            print("Data :: \(data)")

                            // persist the userId in user defaults
                            let userDefaults = UserDefaults.standard
                            userDefaults.setValue(id, forKey: "userid")
                            userDefaults.setValue(username, forKey: "username")
                            //userDefaults.setValue(firstname, forKey: "firstname")
                            //userDefaults.setValue(lastname, forKey: "lastname")
                            userDefaults.setValue(self.email12, forKey: "email")
                            //userDefaults.setValue(contact, forKey: "contact")
                            userDefaults.synchronize()
                            
                            let tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "tabBarController")
                            //self.navigationController?.pushViewController(tabBarController, animated: true)
                            self.present(tabBarController, animated: true, completion: nil)

                        } else {
                            self.showError(message: "Invalid email or pasword")
                       }
                    })
            }
        }
    
    
}
